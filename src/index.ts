import express from 'express';
import bodyParser from 'body-parser';
import getHealthcheck from './api/healthcheck/get';
import setAddressesResolve from './api/addresses/resolve/post';
import setTransactionsCreate from './api/transactions/create/post';

// Create application/json parser
const jsonParser = bodyParser.json();

const app = express()
	.get('/healthcheck', getHealthcheck)
	.post('/addresses/resolve', jsonParser, setAddressesResolve)
	.post('/transactions/create', jsonParser, setTransactionsCreate);

export default app;

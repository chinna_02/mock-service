// Mock endpoint for https://developer.avalara.com/api-reference/avatax/rest/v2/methods/Addresses/ResolveAddress/
const setAddressesResolve = (request: any, response: any) => {
	const address = request.body;
	const coordinates = {
		latitude: 47.613926,
		longitude: -122.201422
	};

	response.json({
		address,
		validatedAddresses: [
			{
				addressType: 'StreetOrResidentalAddress',
				line1: address.line1.toUpperCase(),
				line2: '',
				line3: '',
				city: address.city.toUpperCase(),
				region: address.region,
				country: address.country,
				postalCode: `${address.postalCode}-1234`,
				latitude: coordinates.latitude,
				longitude: coordinates.longitude
			}
		],
		coordinates,
		resolutionQuality: 'Intersection',
		taxAuthorities: [{}]
	});
};

export default setAddressesResolve;

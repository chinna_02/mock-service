const getHealthcheck = (_: any, response: any) => {
	response.send({ version: 0.1 });
};

export default getHealthcheck;

const responseMockJson = {
	id: 123456789,
	code: 'b9dbdc68-test-test-test-b961b1db5a73',
	companyId: 12345,
	status: 'Committed',
	exchangeRateCurrencyCode: 'USD',
	entityUseCode: '',
	customerVendorCode: 'ABC',
	exemptNo: '',
	reconciled: true,
	locationCode: 'DEFAULT',
	salespersonCode: 'DEF',
	taxOverrideType: 'None',
	taxOverrideAmount: 0,
	taxOverrideReason: '',
	totalExempt: 0,
	totalDiscount: 0,
	adjustmentReason: 'NotAdjusted',
	adjustmentDescription: '',
	locked: false,
	version: 0,
	originAddressId: 123456789,
	destinationAddressId: 123456789,
	exchangeRate: 2,
	isSellerImporterOfRecord: false
};

const lineTemplate = {
	id: 123456789,
	transactionId: 123456789,
	boundaryOverrideId: 0,
	entityUseCode: '',
	destinationAddressId: 12345,
	originAddressId: 123456789,
	discountAmount: 0,
	discountTypeId: 0,
	exemptAmount: 0,
	exemptCertId: 0,
	exemptNo: '',
	isItemTaxable: true,
	isSSTP: false,
	ref1: 'Note',
	revAccount: '',
	sourcing: 'Destination',
	taxEngine: '',
	taxOverrideType: 'None',
	taxOverrideAmount: 0,
	taxOverrideReason: '',
	taxIncluded: false,
	details: [
		{
			id: 123456789,
			transactionLineId: 123456789,
			transactionId: 123456789,
			addressId: 12345,
			stateFIPS: '06',
			exemptAmount: 0,
			exemptReasonId: 4,
			inState: false,
			jurisCode: '06',
			jurisName: 'WASHINGTON',
			jurisdictionId: 5000531,
			signatureCode: 'AGAM',
			stateAssignedNo: '',
			jurisType: 'STA',
			nonTaxableAmount: 0,
			nonTaxableRuleId: 0,
			nonTaxableType: 'BaseRule',
			rate: 0.101, // EP requires this attribute and is saved in EP for every transaction
			rateRuleId: 1321915,
			rateSourceId: 3,
			serCode: '',
			sourcing: 'Destination',
			taxType: 'Sales',
			taxName: 'WA STATE TAX',
			taxAuthorityTypeId: 45,
			taxRegionId: 2127184,
			taxOverride: 0,
			rateType: 'General',
			taxableUnits: 62.5,
			nonTaxableUnits: 1000,
			exemptUnits: 62.5,
			reportingTaxableUnits: 125,
			reportingNonTaxableUnits: 2000,
			reportingExemptUnits: 125,
			reportingTax: 125,
			reportingTaxCalculated: 125,
			tax: 0,
			taxableAmount: 0,
			taxCalculated: 0,
			region: '',
			country: ''
		}
	],
	vatNumberTypeId: 0
};

const generateResponseItem = (lineItem: any, date: string, taxDate: string, region: string, country: string) => {
	const templateItem = JSON.parse(JSON.stringify(lineTemplate));
	const {
		amount,
		description,
		itemCode,
		number,
		quantity,
		taxCode
	} = lineItem;
	const lineAmount = quantity * amount;
	// Default tax amount to Seattle tax of 10.1
	const taxAmount = 10.1 / 100 * lineAmount;
	const [tax, taxCalculated, taxableAmount] = [taxAmount, taxAmount, lineAmount];

	templateItem.details[0] = {
		...lineTemplate.details[0],
		tax,
		taxableAmount,
		taxCalculated,
		region,
		country,
		taxName: `${region} STATE TAX`
	};

	return {
		description,
		itemCode,
		lineAmount,
		lineNumber: number,
		quantity,
		date,
		tax,
		taxableAmount,
		taxCalculated,
		taxCode,
		taxDate,
		...templateItem
	};
};

// Mock endpoint for https://developer.avalara.com/api-reference/avatax/rest/v2/methods/Transactions/CreateTransaction/
const setTransactionsCreate = (request: any, response: any) => {
	const {
		addresses: { singleLocation },
		date,
		description,
		currencyCode,
		customerCode,
		lines,
		type
	} = request.body;

	const {
		region,
		country
	} = singleLocation;

	const responseLines = lines.map((lineItem: any) => generateResponseItem(lineItem, date, `${date}T00:00:00+00:00`, region, country));
	const totalTaxable = Number(Number(responseLines.reduce((accumulator: number, responseLine: any) => { // eslint-disable-line unicorn/no-array-reduce
		return accumulator + Number(responseLine.lineAmount);
	}, 0)).toFixed(2));
	const totalTax = Number(Number(responseLines.reduce((accumulator: number, responseLine: any) => { // eslint-disable-line unicorn/no-array-reduce
		return accumulator + Number(responseLine.tax);
	}, 0)).toFixed(2));

	response.json({
		...responseMockJson,
		addresses: [
			{
				id: 0,
				transactionId: 0,
				boundaryLevel: 'Address',
				taxRegionId: 0,
				...singleLocation
			}
		],
		region,
		country,
		customerCode,
		currencyCode,
		date,
		description,
		exchangeRateEffectiveDate: date,
		lines: responseLines,
		taxDate: `${date}T00:00:00+00:00`,
		taxDetailsByTaxType: [
			{
				taxType: 'SalesAndUse',
				totalExempt: 0.05,
				totalNonTaxable: 0,
				totalTax,
				totalTaxable
			}
		],
		totalAmount: totalTaxable,
		totalTax,
		totalTaxable,
		totalTaxCalculated: totalTax,
		type
	});
};

export default setTransactionsCreate;

# tpci-mock-services

## Description
---
Mock 3rd party service endpoints for Avalara, Cybersource and Paypal. Enables continuous development in the event that a 3rd party service is down and to be used with performance testing.

## Prerequisites
---
- [Node.js](1) 12 or greater

## Installing
---

In the main directory, run:

 `npm ci`

## Development
---

To run locally, run:

`npm start`

The API will now be accessible at `localhost:8081`

Libary for mocking api endpoints [connect-api-mocker](https://www.npmjs.com/package/connect-api-mocker)

**Endpoints**

Healthcheck

* GET /healthcheck

    Returns the current version of tpci-mock-services


Avalara

* POST /addresses/resolve

    Replaces the address validation endpoint used in the api lambda layer.
	- Response will return the same address send in the request.
	- Replace env variable `AVALARA_URL` with the mock service url.
	- `https://sandbox-rest.avatax.com/api/v2/addresses/resolve` -> `https://mocks.api.pokemoncenter.com/addresses/resolve`
	- [ResolveAddress API Doc](https://developer.avalara.com/api-reference/avatax/rest/v2/methods/Addresses/ResolveAddress/)

* POST /transactions/create

    Replaces the tax calculation endpoint used by EP.
	- Response will return generic data except for total and tax calculations and request specific data.
	- All calculations are using a default tax rate of 10.1%
	- [CreateTransaction API Doc](https://developer.avalara.com/api-reference/avatax/rest/v2/methods/Transactions/CreateTransaction/)


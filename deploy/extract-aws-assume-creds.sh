#!/bin/bash

if ["$STAGE" ="dev"];
then

EKS_KUBECTL_ROLE_ARN="arn:aws:iam::814445629751:role/K8SStackStack-EksclusterMastersRoleD85283BE-X2TQI3LEP2NI --role-session-name codebuild-kubectl --duration-seconds 900)"

echo "Setting Environment Variables related to AWS CLI for Kube Config Setup "          

CREDENTIALS=$(aws sts assume-role --role-arn $EKS_KUBECTL_ROLE_ARN --role-session-name codebuild-kubectl --duration-seconds 900)

export AWS_ACCESS_KEY_ID="$(echo ${CREDENTIALS} | jq -r '.Credentials.AccessKeyId')"

export AWS_SECRET_ACCESS_KEY="$(echo ${CREDENTIALS} | jq -r '.Credentials.SecretAccessKey')"

export AWS_SESSION_TOKEN="$(echo ${CREDENTIALS} | jq -r '.Credentials.SessionToken')"

export AWS_EXPIRATION=$(echo ${CREDENTIALS} | jq -r '.Credentials.Expiration')

#Setup kubectl with our EKS Cluster              

echo "Update Kube Config" 

aws eks update-kubeconfig --name $EKS_CLUSTER_NAME --region ap-south-1 

kubectl get pods --all-namespaces

kubectl create -f deploy/pod.yaml

else 

echo "stage is stage"

fi
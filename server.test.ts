/* eslint-env jest */
import app from './src/index';
import supertest from 'supertest';

describe('GET /healthcheck', () => {
	test('should return 200 and version', async () => {
		await supertest(app)
			.get('/healthcheck')
			.expect(200)
			.then((response: any) => {
				expect(response.body).toBeTruthy();
				expect(response.body).toEqual({ version: 0.1 });
			});
	});
});

describe('POST /address/resolve', () => {
	test('should return address in Avalara response format', async () => {
		const testAddress = {
			textCase: 'Upper',
			line1: '821 2nd Ave N',
			city: 'Seattle',
			region: 'WA',
			country: 'US',
			postalCode: '98104'
		};
		const expectedValidatedAddress = {
			addressType: 'StreetOrResidentalAddress',
			line1: testAddress.line1.toUpperCase(),
			line2: '',
			line3: '',
			city: testAddress.city.toUpperCase(),
			region: testAddress.region,
			country: testAddress.country,
			postalCode: `${testAddress.postalCode}-1234`,
			latitude: 47.613926,
			longitude: -122.201422
		};

		await supertest(app)
			.post('/addresses/resolve')
			.send(testAddress)
			.expect(200)
			.then((response: any) => {
				expect(response.body).toBeTruthy();

				expect(response.body.address).toEqual(testAddress);
				expect(response.body.validatedAddresses[0]).toEqual(expectedValidatedAddress);
			});
	});
});

describe('POST /transactions/create', () => {
	const mockTransaction = {
		lines: [
			{
				number: '1',
				quantity: 1,
				amount: 80,
				taxCode: 'TestTaxCode',
				itemCode: 'TestItemCode',
				description: 'TestLineDescription'
			}
		],
		type: 'SalesInvoice',
		companyCode: 'TestCompanyCode',
		date: '2021-02-18',
		customerCode: 'TestCustomer',
		purchaseOrderNo: '2021-02-18-001',
		addresses: {
			singleLocation: {
				line1: '821 2nd Ave',
				city: 'Seattle',
				region: 'WA',
				country: 'US',
				postalCode: '98104'
			}
		},
		commit: true,
		currencyCode: 'USD',
		description: 'TestDescription'
	};

	test('should return single item with correct tax and totalTaxable amounts', async () => {
		await supertest(app)
			.post('/transactions/create')
			.send(mockTransaction)
			.expect(200)
			.then((response: any) => {
				expect(response.body).toBeTruthy();

				expect(response.body.lines.length).toEqual(1);
				expect(response.body.totalTaxable).toEqual(80);
				expect(response.body.totalTax).toEqual(8.08);
			});
	});

	test('should return multi items with correct tax and totalTaxable amounts', async () => {
		const multiItemTransaction = mockTransaction;
		multiItemTransaction.lines.push({
			number: '2',
			quantity: 2,
			amount: 25,
			taxCode: 'TestTaxCode2',
			itemCode: 'TestItemCode2',
			description: 'TestSecondLineItemDescription'
		});

		await supertest(app)
			.post('/transactions/create')
			.send(multiItemTransaction)
			.expect(200)
			.then((response: any) => {
				expect(response.body).toBeTruthy();

				expect(response.body.lines.length).toEqual(2);
				expect(response.body.totalTaxable).toEqual(130);
				expect(response.body.totalTax).toEqual(13.13);
			});
	});
});
